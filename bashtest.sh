#!/bin/bash
DATABASE=$1
full_path=$(realpath $0)
dir_path=$(dirname $full_path)
echo $full_path
echo $dir_path

if [ -f .env ]
then
  export $(cat $dir_path/.env | sed 's/#.*//g' | xargs)
fi

USR_VAR=USR_${DATABASE^^}
USERNAME=${!USR_VAR}
echo $USERNAME


PWD_VAR=PWD_${DATABASE^^}
PASSWORD=${!PWD_VAR}
echo $PASSWORD