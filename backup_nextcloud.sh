#!/bin/bash
SECONDS=0
echo '###### 1 - backup dir ==> $1 ######'
BACKUP_DIR=$1
DB_TYPE=$2
DATABASE_NAME=$3
DBPORT=$4
CLEANUP_SCRIPT=$5
FINISHED_WITH_ERROR=0

echo "starting execution of backup_nextcloud.sh"

if [  "$DB_TYPE" != "MYSQL" -a "$DB_TYPE" != "POSTGRES" ]
then
    echo "DB_TYPE most be MYSQL or POSTGRES"
    exit 1
fi

echo ''
echo '###### 2 - enabling maintenance ######'
sudo -u www-data php /var/www/nextcloud/occ maintenance:mode --on
RESULT=$?
if [ $RESULT -ne 0 ]; then
    FINISHED_WITH_ERROR=1
fi
eval "echo Elapsed time: $(date -ud "@$SECONDS" +'$((%s/3600/24)) days %H hr %M min %S sec')"
echo ''

echo '###### 3 - syncyng /var/www/nextcloud/ to $1/nextcloud-dirbkp ######'
sudo rsync -AavxL /var/www/nextcloud/ $BACKUP_DIR/nextcloud-dirbkp
RESULT=$?
if [ $RESULT -ne 0 ]; then
    FINISHED_WITH_ERROR=1
fi
eval "echo Elapsed time: $(date -ud "@$SECONDS" +'$((%s/3600/24)) days %H hr %M min %S sec')"
echo ''

echo '###### 4 - creating $DB_TYPE dump for database nextcloud ######'
##time sudo mysqldump --single-transaction -u root -p$1 nextcloud > $BACKUP_DIR/mysqldumps/$(date +'%Y%m%d')_nextcloud-sql.bak
if [ "$DB_TYPE" == "MYSQL" ]
then
    mkdir -p $BACKUP_DIR/mysqldumps
    /opt/backup_scripts/mysql_backup.sh localhost $DATABASE_NAME $DBPORT $BACKUP_DIR/mysqldumps 30 $CLEANUP_SCRIPT
else
    mkdir -p $BACKUP_DIR/postgresdumps
    /opt/backup_scripts/postgres_backup.sh localhost $DATABASE_NAME $DBPORT $BACKUP_DIR/postgresdumps 30 $CLEANUP_SCRIPT
fi
RESULT=$?
if [ $RESULT -ne 0 ]; then
    FINISHED_WITH_ERROR=1
fi

echo ''
echo '###### 5 - disabling maintenance ######'
sudo -u www-data php /var/www/nextcloud/occ maintenance:mode --off
RESULT=$?
if [ $RESULT -ne 0 ]; then
    FINISHED_WITH_ERROR=1
fi
eval "echo Elapsed time: $(date -ud "@$SECONDS" +'$((%s/3600/24)) days %H hr %M min %S sec')"
echo ''

echo '###### 6 - syncyng users data from /mnt/dietpi_userdata/nextcloud_data $BACKUP_DIR/nextcloud_databkp ######'
sudo rsync -AavxL /mnt/dietpi_userdata/nextcloud_data $BACKUP_DIR/nextcloud_databkp > $BACKUP_DIR/logs/users_data_$(date +'%Y%m%d').log
RESULT=$?
if [ $RESULT -ne 0 ]; then
    FINISHED_WITH_ERROR=1
fi
eval "echo Elapsed time: $(date -ud "@$SECONDS" +'$((%s/3600/24)) days %H hr %M min %S sec')"
echo ''

echo "end of execution of backup_nextcloud.sh"
if [ $FINISHED_WITH_ERROR -ne 0 ]; then
  exit 1
fi

