#!/bin/bash
echo "starting execution of cron_backup_nextcloud.sh"

SECONDS=0
BACKUP_BASE_DIR=$1
DB_TYPE=$2
DATABASE_NAME=$3
DB_PORT=$4
CLEANUP_SCRIPT=$5

FINISHED_WITH_ERROR=0

echo "DB_TYPE = $DB_TYPE"
echo "DATABASE_NAME = $DATABASE_NAME"
echo "DB_PORT = $DB_PORT"
echo "BACKUP_BASE_DIR = $BACKUP_BASE_DIR"


if [  "$DB_TYPE" != "MYSQL" -a "$DB_TYPE" != "POSTGRES" ]
then
    echo "DB_TYPE most be MYSQL or POSTGRES"
    exit 1
fi

if [ "$DB_TYPE" == "MYSQL" ]
then
    if test -z "$DB_PORT" 
    then
        echo "DBPORT is empty, assuming port 3306"
        DB_PORT=3306
    fi
else
    if test -z "$DB_PORT" 
    then
        echo "DBPORT is empty, assuming port 5432"
        DB_PORT=5432
    fi
fi



cd $BACKUP_BASE_DIR
mkdir -p ./bkp_nextcloud
BACKUP_DIR=$1/bkp_nextcloud
echo "BACKUP_DIR = $BACKUP_DIR" 

mkdir -p $BACKUP_DIR/logs

/opt/backup_scripts/backup_nextcloud.sh $BACKUP_DIR $DB_TYPE $DATABASE_NAME $DB_PORT $CLEANUP_SCRIPT > $BACKUP_DIR/logs/backup_nextcloud_$(date +'%Y%m%d').log
RESULT=$?
if [ $RESULT -ne 0 ]; then
    FINISHED_WITH_ERROR=1
fi
eval "echo Backup Elapsed time: $(date -ud "@$SECONDS" +'$((%s/3600/24)) days %H hr %M min %S sec')"
echo ''

echo '###### Cleaning up oldests backup_nextcloud_xxxxxxxx.log files`  ######'
sudo /usr/bin/python3 $CLEANUP_SCRIPT -p $BACKUP_DIR/logs -n 30 -x backup_nextcloud_\\d{8}.log
RESULT=$?
if [ $RESULT -ne 0 ]; then
    FINISHED_WITH_ERROR=1
fi
eval "echo Elapsed time: $(date -ud "@$SECONDS" +'$((%s/3600/24)) days %H hr %M min %S sec')"
echo ''

echo '###### Cleaning up oldests users_data_xxxxxxxx.log logs ######'
sudo /usr/bin/python3 $CLEANUP_SCRIPT -p $BACKUP_DIR/logs -n 30 -x users_data_\\d{8}.log
RESULT=$?
if [ $RESULT -ne 0 ]; then
    FINISHED_WITH_ERROR=1
fi
eval "echo Total Elapsed time: $(date -ud "@$SECONDS" +'$((%s/3600/24)) days %H hr %M min %S sec')"
echo ''
echo "end of execution of cron_backup_nextcloud.sh"
if [ $FINISHED_WITH_ERROR -ne 0 ]; then
  exit 1
fi

