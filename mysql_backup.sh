#!/bin/bash
SECONDS=0
MYSQLHOST=$1
DATABASE=$2
DBPORT=$3
BACKUPDIR=$4
DAYS_RETENTION=$5
CLEANUP_SCRIPT=$6

FINISHED_WITH_ERROR=0

if [[ -z "$CLEANUP_SCRIPT" ]]; then
   # $var is empty, do what you want
   CLEANUP_SCRIPT="/opt/cleanup_backup_files/cleanup_files.py"
fi

if [[ -z "$DAYS_RETENTION" ]]; then
   # $var is empty, do what you want
   DAYS_RETENTION=30
fi

full_path=$(realpath $0)
dir_path=$(dirname $full_path)
echo "dir_path=$dir_path"

if [ -f  $dir_path/.env ]
then
  #echo ">>>>>> reading variables from $dir_path/.env"
  export $(cat $dir_path/.env | sed 's/#.*//g' | xargs)
else
  echo ">>>>>> variables file $dir_path/.env not found"
fi

if [ -z "$DBPORT" ]
then
      echo "DBPORT is empty, assuming port 3306"
      DBPORT=3306
fi

USR_VAR=USR_${DATABASE^^}
USERNAME=${!USR_VAR}

PWD_VAR=PWD_${DATABASE^^}
PASSWORD=${!PWD_VAR}

mkdir -p $BACKUPDIR

DUMPFILENAME="$BACKUPDIR/$DATABASE"_$(date +'%Y%m%d').dump

echo ">>>>>> creating mysql dump for database $DATABASE to $DUMPFILENAME"
sudo mysqldump -h $MYSQLHOST --port $DBPORT --single-transaction -u $USERNAME -p$PASSWORD $DATABASE > $DUMPFILENAME

RESULT_SQL_DUMP=$?

if [ $RESULT_SQL_DUMP -eq 0 ]; then
  echo ">>>>>> compressing $DUMPFILENAME to $DUMPFILENAME.tgz"
  tar cfz $DUMPFILENAME.tgz $DUMPFILENAME

  RESULT=$?
  if [ $RESULT -eq 0 ]; then
    echo "removing $DUMPFILENAME"
    rm $DUMPFILENAME
  else
    echo "ERROR: fail to compress $DUMPFILENAME"
    FINISHED_WITH_ERROR=1
  fi

  eval "echo Elapsed time: $(date -ud "@$SECONDS" +'$((%s/3600/24)) days %H hr %M min %S sec')"
  echo ""


  if [ -f  $CLEANUP_SCRIPT ]
  then
    echo ">>>>>>>> Cleaning up oldests "@$DATABASE"_xxxxxxxx..dump.tgz files"
    REGEX_DUMPFILENAME="$DATABASE"_\\d{8}.dump.tgz
    sudo /usr/bin/python3 $CLEANUP_SCRIPT -p $BACKUPDIR -n $DAYS_RETENTION -x $REGEX_DUMPFILENAME

    RESULT=$?
    if [ $RESULT -ne 0 ]; then
      FINISHED_WITH_ERROR=1
    fi

    eval "echo Elapsed time: $(date -ud "@$SECONDS" +'$((%s/3600/24)) days %H hr %M min %S sec')"
    echo ""
  else
    echo ">>>>>> Cleaning script $CLEANUP_SCRIPT not found."
  fi
 
else
  echo ">>>>>> ERROR: creating mysql dump for database $DATABASE to $DUMPFILENAME"
  FINISHED_WITH_ERROR=1
fi

if [ $FINISHED_WITH_ERROR -ne 0 ]; then
  exit 1
fi
